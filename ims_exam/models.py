from django.db import models
from ims_base.models import AbstractBaseDetail, AbstractLog
from reusable_models import get_model_from_string

Batch = get_model_from_string("BATCH")
Subject = get_model_from_string("SUBJECT")
BatchStudent = get_model_from_string("BATCH_STUDENT")
Faculty = get_model_from_string("FACULTY")
Room = get_model_from_string("ROOM")


class ExamType(AbstractBaseDetail):
    duration_in_minutes = models.PositiveSmallIntegerField(null=True, blank=True)
    max_marks = models.PositiveSmallIntegerField(null=True, blank=True)
    marks_to_qualify = models.PositiveSmallIntegerField(null=True, blank=True)
    allow_modification = models.BooleanField(default=False)


class Exam(AbstractLog):
    name = models.CharField(max_length=255, unique=True)
    exam_type = models.ForeignKey(
        ExamType, on_delete=models.CASCADE, null=True, blank=True
    )
    batch = models.ForeignKey(Batch, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    max_marks = models.PositiveSmallIntegerField(null=True, blank=True)
    marks_to_qualify = models.PositiveSmallIntegerField(null=True, blank=True)
    date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    is_submitted = models.BooleanField(default=False)

    def __str__(self) -> str:
        return self.name


class ExamResult(AbstractLog):
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE)
    batch_student = models.ForeignKey(BatchStudent, on_delete=models.CASCADE)
    marks = models.PositiveSmallIntegerField()
    is_present = models.BooleanField(default=False)
    remarks = models.TextField(null=True, blank=True)

    def __str__(self) -> str:
        return self.registration_id
