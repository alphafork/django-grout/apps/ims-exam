import datetime

from ims_base.serializers import BaseModelSerializer
from rest_framework import serializers
from reusable_models import get_model_from_string

from .models import Exam, ExamResult

StudentRegistration = get_model_from_string("STUDENT_REGISTRATION")


class ExamTypeSerializer(BaseModelSerializer):
    def validate(self, attrs):
        validated_data = super().validate(attrs)
        validation_errors = {}
        if not validated_data["allow_modification"]:
            for field in ["duration_in_minutes", "max_marks", "marks_to_qualify"]:
                if not validated_data.get(field, None):
                    validation_errors.update({field: "This field is required."})
        if validation_errors:
            raise serializers.ValidationError(validation_errors)
        return validated_data


class ExamSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        fields = [
            "id",
            "name",
            "exam_type",
            "batch",
            "subject",
            "max_marks",
            "marks_to_qualify",
            "date",
            "start_time",
            "end_time",
            "is_submitted",
        ]

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        exam_type = validated_data["exam_type"]
        if not exam_type.allow_modification:
            exam_details_fields = [
                "max_marks",
                "marks_to_qualify",
            ]
            for field in exam_details_fields:
                if getattr(exam_type, field) != validated_data[field]:
                    raise serializers.ValidationError(
                        {field: f"{field} is not allowed to update."}
                    )

            end_time = validated_data["end_time"]
            if end_time:
                start_time = validated_data["start_time"]
                end_time_timedelta = datetime.timedelta(
                    hours=end_time.hour,
                    minutes=end_time.minute,
                    seconds=end_time.second,
                )
                start_time_timedelta = datetime.timedelta(
                    hours=start_time.hour,
                    minutes=start_time.minute,
                    seconds=start_time.second,
                )
                allowed_duration = datetime.timedelta(
                    minutes=exam_type.duration_in_minutes
                )
                if end_time_timedelta - start_time_timedelta != allowed_duration:
                    raise serializers.ValidationError(
                        {
                            "end_time": (
                                "Allowed duration is "
                                + str(exam_type.duration_in_minutes)
                                + "minutes"
                            ),
                        }
                    )
        if (
            self.instance
            and not self.context["request"]
            .user.groups.filter(name__in=["Manager", "Admin"])
            .exists()
            and self.instance.is_submitted
        ):
            raise serializers.ValidationError(
                {"is_submitted": "Operation not permitted for this user"}
            )

        return validated_data


class ExamResultSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        model = ExamResult

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        if not validated_data["is_present"] and validated_data["marks"] > 0:
            raise serializers.ValidationError(
                {"marks": "Can not add marks for absent student."}
            )
        exam = validated_data["exam"]
        if validated_data["marks"] > exam.max_marks:
            raise serializers.ValidationError(
                {"marks": f"Max mark is {exam.max_marks}."}
            )
        return validated_data


class BatchExamResultSerializer(BaseModelSerializer):
    exam_result = serializers.ListField(
        write_only=True,
        child=ExamResultSerializer(),
    )
    exam = serializers.PrimaryKeyRelatedField(
        write_only=True,
        queryset=Exam.objects.all(),
    )

    class Meta(BaseModelSerializer.Meta):
        fields = ["exam", "exam_result"]

    def validate(self, attrs):
        validation_errors = {}
        validated_data = super().validate(attrs)
        exam = validated_data.get("exam", None)
        exam_result = validated_data.get("exam_result", [])
        if not exam:
            validation_errors.update({"exam": "This field can not be empty."})
        if not exam_result:
            validation_errors.update({"exam_result": "This field can not be empty."})
        for student_result in exam_result:
            if student_result["exam"] != exam:
                validation_errors.update(
                    {
                        "exam_result": student_result["batch_student"].__str__()
                        + "entry conflicts with given exam."
                    }
                )
        if validation_errors:
            raise serializers.ValidationError(validation_errors)
        return validated_data

    def create(self, validated_data):
        exam_result = validated_data.pop("exam_result", [])
        exam = validated_data.pop("exam", [])
        old_exam_results = ExamResult.objects.filter(exam=exam)
        if old_exam_results.exists():
            old_exam_results.delete()
        for student_result in exam_result:
            ExamResult.objects.create(**student_result)
        return True


class RegistrationExamResultSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        model = StudentRegistration

    def to_representation(self, instance):
        registration_exam_result = ExamResult.objects.filter(
            batch_student__registration=instance
        )

        registration_results = []
        for exam_result in registration_exam_result:
            marks = exam_result.marks
            max_marks = exam_result.exam.max_marks
            marks_to_qualify = exam_result.exam.marks_to_qualify
            percentage = f"{round((marks*100)/max_marks, 2)}%"
            status = "passed" if marks >= marks_to_qualify else "failed"
            registration_result = {
                "exam": exam_result.exam.name,
                "marks": marks,
                "marks_to_qualify": marks_to_qualify,
                "max_marks": max_marks,
                "percentage": percentage,
                "status": status,
                "is_present": exam_result.is_present,
                "remarks": exam_result.remarks,
            }
            registration_results.append(registration_result)

        if registration_exam_result:
            return {"exam_results": registration_results}
        else:
            return {"exam_results": "Not Applicable"}
