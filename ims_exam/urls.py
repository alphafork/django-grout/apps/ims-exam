from django.urls import include, path
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from .apis import BatchExamResultAPI, RegistrationExamResultAPI

router = routers.SimpleRouter()
router.register(r"batch-result", BatchExamResultAPI, "batch-exam-result")

urlpatterns = [
    path("registration/<int:pk>/", RegistrationExamResultAPI.as_view()),
    path("", include(router.urls)),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=["json"])
