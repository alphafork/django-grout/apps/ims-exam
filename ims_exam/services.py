from ims_exam.models import ExamResult


def get_course_exam_result(registration, course):
    exam_results = ExamResult.objects.filter(
        batch_student__registration=registration,
        batch_student__batch__course=course,
    )

    registration_results = []
    for exam_result in exam_results:
        marks = exam_result.marks
        max_marks = exam_result.exam.max_marks
        marks_to_qualify = exam_result.exam.marks_to_qualify
        percentage = f"{round((marks*100)/max_marks, 2)}%"
        status = "passed" if marks >= marks_to_qualify else "failed"
        registration_result = {
            "exam": exam_result.exam.name,
            "marks": marks,
            "marks_to_qualify": marks_to_qualify,
            "max_marks": max_marks,
            "percentage": percentage,
            "status": status,
            "is_present": exam_result.is_present,
            "remarks": exam_result.remarks,
        }
        registration_results.append(registration_result)

    if exam_results:
        return {"exam_results": registration_results}
    else:
        return {"exam_results": "Not Applicable"}
