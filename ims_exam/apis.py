from ims_base.apis import BaseAPIViewSet, BaseRetrieveView
from ims_user.apis import UserFilter
from reusable_models import get_model_from_string

from ims_exam.serializers import (
    BatchExamResultSerializer,
    RegistrationExamResultSerializer,
)

from .models import ExamResult

StudentRegistration = get_model_from_string("STUDENT_REGISTRATION")


class BatchExamResultAPI(BaseAPIViewSet):
    serializer_class = BatchExamResultSerializer
    model_class = ExamResult


class RegistrationExamResultAPI(BaseRetrieveView):
    serializer_class = RegistrationExamResultSerializer
    queryset = StudentRegistration.objects.all()


class ExamAPI(BaseAPIViewSet):
    filter_backends = BaseAPIViewSet.filter_backends + [
        UserFilter,
    ]
    obj_user_groups = ["Admin", "Manager"]
    user_lookup_field = "batch__batch_mentor__staff__user"


class ExamResultAPI(BaseAPIViewSet):
    filter_backends = BaseAPIViewSet.filter_backends + [
        UserFilter,
    ]
    obj_user_groups = ["Admin", "Manager"]
    user_lookup_field = "exam__batch__batch_mentor__staff__user"
